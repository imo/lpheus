# lpheus

lpheus is meant as the receiving part for the prosody [mod_http_upload_external](https://modules.prosody.im/mod_http_upload_external.html) module. I began to wrote this as a learning project, but it ended up lying around on my machine for some time now. When I read a post from [Thomas Leister on Mastodon](https://social.tchncs.de/@trashserver/100283535115441492) about the release of [his implementation](https://github.com/ThomasLeister/prosody-filer) I remembered that I have one too.  
Maybe I'll take another look at it and finish it or maybe someone will help me. :)

## Status

In short - the upload part is kinda ugly and currently only tested with cURL, not with prosody.

* Only PUT, GET, HEAD is allowed
* in `location /` a short lua block routes the request, depending on the method
* download is handled by nginx's sendfile (why reinventing the wheel?)
* upload is handled by `upload.lua`
   * the whole data is read into memory, which sucks - #1

## Dependencies

* [openresty](https://openresty.org/en/installation.html)
* [resty-hmac](https://github.com/jkeys089/lua-resty-hmac) - `opm get jkeys089/lua-resty-hmac `
* [resty-string](https://github.com/openresty/lua-resty-string) - `opm get openresty/lua-resty-string` (`resty-hmac` depends on this one, so it should get installed automatically)
* [luafilesystem](https://github.com/spacewander/luafilesystem) - `opm get spacewander/luafilesystem`

## TODO

See [Issues](https://git.kokolor.es/imo/lpheus/issues) for that.
