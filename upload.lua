-- Load modules
local hmac = require('resty.hmac')
local str = require('resty.string')
local lfs = require('lfs_ffi')

-- Read whole request data into variable -> see issue #1
ngx.req.read_body()
local body = ngx.var.request_body

-- Variables
local conf_upath = ngx.var.upload_path
local conf_secret = ngx.var.secret
local req_uri = ngx.var.uri
local req_arg_v = ngx.var.arg_v
local req_head_clength = ngx.req.get_headers()['content-length']
local chunksize = 4096

-- Just for debugging reasons, will be changed or removed
local function debug_logging(msg)
  ngx.log(ngx.STDERR, msg)
end

-- Returning failures
local function return_failure(code, msg)
  ngx.status = code
  ngx.header['Content-Type'] = 'text/html'
  ngx.say(msg)
  ngx.exit(code)
end

-- Check if file exists function
local function file_exists(file)
  local _file = io.open(file, 'rb')

  if _file then _file:close() end

  return _file ~= nil
end

-- Check if dir exists function
local function dir_exists(dir)
  if lfs.attributes(dir, 'mode') == 'directory' then
    return true
  else
    return false
  end
end

-- Check if HMAC is attached to URL
if not req_arg_v then
  return_failure(ngx.HTTP_FORBIDDEN, 'No HMAC')
end

-- Gen hmac
local hmac_sha256 = hmac:new(conf_secret, hmac.ALGOS.SHA256)
local mac = str.to_hex(hmac_sha256:final(req_uri:sub(2, -1) .. ' ' .. req_head_clength))
hmac_sha256:reset()

-- Check if attached hmac matches ours and if not return status 403
if req_arg_v == mac then
  local upload_dir = conf_upath .. req_uri:match('^(%/.*)%/.*%.[a-zA-Z_]+$')

  -- Check if dir exists and if not create it
  if dir_exists(upload_dir) then
    -- Check if file already exists and if yes return status 409
    if file_exists(conf_upath .. req_uri) then
      return_failure(ngx.HTTP_CONFLICT, string.format('%s already exists', req_uri))
    end
  else
    local ok = lfs.mkdir(upload_dir)

    -- If can't create new dir, return status 500
    if not ok then
      return_failure(ngx.HTTP_INTERNAL_SERVER_ERROR, 'Can\'t create new folder')
    end
  end

  -- Create new file and write data to it and return status 201
  local file = io.open(conf_upath .. req_uri, 'w+')
  file:write(body)
  file:close()
  ngx.exit(ngx.HTTP_CREATED)
else
  return_failure(ngx.HTTP_FORBIDDEN, 'Wrong HMAC')
end
